// Get the latest commit hash
const { execSync } = require('child_process');
const fs = require('fs');

// Write targets to file?
// Write spec list?

/**
 * Evaluate if the changes from the current commit contain files that should trigger the e2e
 * test suite.
 */
function shouldRunFullTestSuite() {
  const getCurrentCommitHashCommand = 'git rev-parse --short HEAD';
  const getChangesCommand = 'git diff --name-only HEAD HEAD~1';

  const changePathsThatTriggerTests = [
    'storefronts/_templates/',
    'storefronts/globalrewards/',
  ];

  const changedFiles = execSync(getChangesCommand).toString().trim().split('\n');
  const currentCommitHash = execSync(getCurrentCommitHashCommand).toString().trim();

  let shouldRunTests = false;

  console.log(`Looking for changes that match product paths: ${changePathsThatTriggerTests.join(', ')}`);
  console.log('');
  console.log(`#${currentCommitHash} - Changed files:`);
  console.log(`- ${changedFiles.join('\n- ')}`);
  console.log('');

  for (const testCase of changePathsThatTriggerTests) {
    const matchFound = changedFiles.join(',').indexOf(testCase) !== -1;
    if (matchFound) {
      console.log(`Commit "${currentCommitHash}" contains changes to: "${testCase}". Enabling full test suite.`);
      shouldRunTests = true;
      break;
    }
  }

  if (!shouldRunTests) {
    console.log('No changed files match path. Skipping full test suite.');
  } else {
    fs.writeFileSync('../.env', `RUN_FULL_SUITE=${shouldRunTests}`);
  }
}

shouldRunFullTestSuite();
