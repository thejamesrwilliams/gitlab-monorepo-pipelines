# GitLab Monorepo Pipelines

From `.gitlab-ci.yml` I need to know which subdirectory has been changed from our parent 
`projects`. 

- [ ] Merge requests
- [ ] Merge commits
- [ ] Commits directly on master

```
projects
 ├─> project-a
 │   └── README.md
 ├─> project-b
 │   └── README.md
 ├─> project-c
 │   └── README.md
 ├─> project-d
 │   └── README.md
 └─> project-e
     └── README.md
```
